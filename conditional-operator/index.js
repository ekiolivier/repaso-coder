/*
https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Operators/Conditional_Operator

El operador condicional (ternario) es el único operador en JavaScript que tiene tres operandos. Este operador se usa con frecuencia como atajo para la instrucción if.

condicion ? expresion_1 : expresion_2
si la condición es true ejecuta la expresion_1 sino expresion_2

Todos los valores son verdaderos a menos que se definan como falso (es decir, excepto false, 0, "", null, undefined, y NaN).
*/

let valor_1 = true
let valor_2 = undefined
let valor_3 = 0
let valor_4 = ''
let valor_5 = false


console.log(valor_1 ? 'es verdadero' : 'es falso');
