/*
https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment

La sintaxis de desestructuración es una expresión de JavaScript que permite desempacar valores de arreglos o propiedades de objetos en distintas variables.
*/

let obj = {
    nombre: 'ezequiel',
    apellido: 'olivera',
    mas_datos: {
      lenguaje: 'javascript'
    }
  }
  
  let { nombre, apellido, mas_datos } = obj
  let { lenguaje } = mas_datos
  
  let arr = ['eze', 'olivera']
  let [ name, lastName ] = arr
  
  console.log(`mi nombre y apellido es ${name} ${lastName} y programa en ${lenguaje}`)