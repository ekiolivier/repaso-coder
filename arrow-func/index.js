// () => {...}
// arrow function se puede guardar en variables
const foo = (num) => console.log(num * 2)
const bar = () => {}
foo(10)


let arr = [1,2,3,4]
// se puede usar como parámetros de una función
arr.forEach(foo)

/*
router.get('/', (req, res) => {
  res.json({
    mensaje: 'solicitud exitosa',
    data: 'lo que venga de la query a la BD'
  })
})
*/

function Saludar(name='coderhouse') {
  //if(name === undefined) return new Error('el valor no debe ser undefined')
  //if(name === null) return new Error('el valor debe ser distinto de null')

  return `hola ${name}`
}

Saludar()