/*
let te permite declarar variables limitando su alcance (scope) al bloque donde se está usando, a diferencia de la palabra var la cual define una variable global o local en una función sin importar el ámbito del bloque. MDN
*/

function varTest() {
    var x = 31;
    if (true) {
      var x = 71;  // ¡misma variable!
      console.log(x, 'dentro del if');  // 71
    }
    console.log(x, 'fuera del if');  // 71
  }
  
  function letTest() {
    let x = 31;
    if (true) {
      let x = 71;  // variable diferente
      console.log(x,'solo accede a la x en el ambito del if');  // 71
    }
    console.log(x, 'solo accede a la x en el ambito de la funcion');  // 31
  }
  // llamamos a las funciones
  //varTest();
  letTest();