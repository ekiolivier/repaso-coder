/*
https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Operators/Spread_syntax

La sintaxis extendida o spread syntax permite a un elemento iterable tal como un arreglo o cadena ser expandido en lugares donde cero o más argumentos (para llamadas de función) o elementos (para Array literales) son esperados, o a un objeto ser expandido en lugares donde cero o más pares de valores clave (para literales Tipo Objeto) son esperados.
*/

//con objetos
let user = {
    nombre: 'ezequiel',
    apellido: 'olivera'
  }
  
  let phone_number = {
    phone: '+54 11223344'
  }
  
  let user_with_data = {
    ...user, // nombre: 'ezequiel', apellido: 'olivera'
    ...phone_number, // phone: '+54 11223344'
    age: 34
  }
  
  let copia_de_user = {...user_with_data}
  
  console.log(user_with_data,'Concatenar y añadir nuevas propiedades a un objeto')
  console.log(copia_de_user,'copia de objeto')
  
  // arrays
  
  let arr1 = [1,2,3]
  let arr2 = [4,5,6,7,8]
  
  let arr3 = [...arr1, ...arr2]
  
  console.log(arr3)
  
  //funciones
  const sumarNumeros = (a, b, c, d, f) => {
      console.log(a + b + c + d +f , 'sumarNumeros');
  }
  
  sumarNumeros(...arr2); // (...arr1 )--> (1,2,3)
  sumarNumeros(4,5,6)
  
  
  