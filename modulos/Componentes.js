/*
Export nombrados

function Button() {
    return 'hola soy un boton'
}
export { Button }

export const Modal = () => 'Hola soy un modal'

export function NavBar() {
    return 'Hola soy un nabvar'
}
*/

/*
Export por defecto

*/
export default function Button() {
    return 'hola soy un boton'
}