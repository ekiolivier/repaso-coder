/*
El operador Rest disponible en Javascript ES6 sirve para recibir cualquier número de parámetros en una función en forma de array.
Sirve para obtener cualquier número de parámetros de una forma estructurada, mediante un array de valores.
*/

function log(...arg) {
    //Siempre te llega un array
    console.log(arg)
    return arg.forEach(e => console.log(e))
  }
  log({name: 'eze', apellido: 'olivera'})